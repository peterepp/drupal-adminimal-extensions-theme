<?php

/**
 * Display a single views grouping.
 */
function adminimal_ext_views_view_grouping($vars) {
  $view = $vars['view'];
  $title = $vars['title'];
  $content = $vars['content'];

  $class = trim(strtolower(strip_tags($title)));
  $class = preg_replace('/[^a-zA-Z0-9]+/si', '-', $class);
  $class = trim($class, ' -');

  $output = '<div class="view-grouping ' . $class . '">';
  $output .= '<div class="view-grouping-header">' . $title . '</div>';
  $output .= '<div class="view-grouping-content">' . $content . '</div>' ;
  $output .= '</div>';

  return $output;
}

/**
 * Implements theme_breadcrumb().
 *
 * Remove breadcrumb separator.
 */
function adminimal_ext_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $output = '';
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb"><ul>
    <li>' . implode('</li><li>', $breadcrumb) . '</li></ul></div>';
  }
  return $output;
}
